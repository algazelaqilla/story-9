from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import *
from .views import *
from django.contrib.auth.models import User
from django.http import request
from django.contrib.messages.api import get_messages
# Create your tests here.
class LoginTestCase(TestCase):

    def test_url_landing(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_login(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_invalid_login(self):
        response = self.client.post('/login',{'username':'', 'password':''}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_valid_login(self):
        test = 'test'
        user = User.objects.create_user(test, test, test)
        user.save()
        response = self.client.post('/login',{'username':test, 'password':test}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')


    def test_url_valid_signup(self):
        test = 'test'
        response = self.client.post('/signup',{'username':test, 'email':test, 'password2':test}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_invalid_signup(self):
        test = ''
        response = self.client.post('/signup',{'username':test, 'email':test, 'password2':test}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_failed_url_user(self):
        response = self.client.get('/user/', follow= True)
        self.assertEqual(response.status_code, 200)

    def test_success_url_user(self):
        test = 'test'
        user = User.objects.create_user(test, test, test)
        self.client.login(username=test, password=test)
        response = self.client.get('/user/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_resolved_landing(self):
        url = reverse('login:landing')
        self.assertEqual(resolve(url).func, landing)

    def test_url_is_resolved_login(self):
        url = reverse('login:login')
        self.assertEqual(resolve(url).func, login)

    def test_url_is_resolved_logout(self):
        url = reverse('login:logout')
        self.assertEqual(resolve(url).func, out)

    def test_url_is_resolved_signup(self):
        url = reverse('login:signup')
        self.assertEqual(resolve(url).func, signup)

    def test_url_is_resolved_user(self):
        url = reverse('login:user')
        self.assertEqual(resolve(url).func, user)

    
    