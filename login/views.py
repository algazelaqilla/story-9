from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib import messages
from django.contrib.auth.models import User

# Create your views here.
def login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            # Redirect to a success page.
            return redirect('/')
        else:
            # Return an 'invalid login' error message.
            messages.error(request, 'username or password is incorrect')

    return render(request, 'login.html')

def signup(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password2')
        email = request.POST.get('email')
        if not request.POST.get('password1') == request.POST.get('password2'):
            messages.error(request, 'Re-entered password does not match!')
        else:
            if not User.objects.filter(username=username).exists():
                user = User.objects.create_user(username, email, password)
                user.save()
                return redirect('/login')
            else:
                # Return an 'invalid login' error message.
                messages.error(request, 'username/email already exist')
    return render(request, 'signup.html')

def landing(request):
    return render(request, 'landing.html')

def user(request):
    if not request.user.is_authenticated:
        return redirect('/login')
    else:
        username = request.user.username
        if len(username)%2 == 0:
            username = 'putin'
        else:
            username = 'jimmy'    
        return render(request, 'user.html', {'username':username})
    

def out(request):
	logout(request)
	return redirect('/login')

    